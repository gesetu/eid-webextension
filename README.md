GesEtu eID Helper Documentation 
*******************************

eID Helper Web Extension for GesEtu to use with the module GesEtuEID.

See [Tags](https://gitlab.com/gesetu/eid-webextension/tags) for releases.
