/** Request GesEtuEID to read the BeID.
 */
function GesEtuEidOnClick(event)
{
	//console.log("GesEtuEidOnClick");
	
	if ( event.target.dispatchEvent(new CustomEvent("beidreadbegin", {cancelable: true})) ){
		var requestURL = "http://localhost:8080/eid";
		var request = new XMLHttpRequest();
		
		//request.responseType = 'json';
		request.onload = function(){
			var responseEvent;
			//console.debug("status="+this.status);
			// On success
			if( this.status == 200 ){
				//console.debug(this.response);
				//data = this.response;
				responseEvent = new CustomEvent("beidreaded", { "detail": this.response });
			}
			// On fail
			else{
				var error = null;
				if( this.status == 251 ){
					error = {error: this.status, message : "Invalid card"};
				}
				else if( this.status == 252 ){
					error = {error: this.status, message : "No card"};
				}
				else{
					error = {error: this.status, message : ""};
				}
				responseEvent = new CustomEvent("beidreaderror", { "detail": JSON.stringify(error) });
			}
			// Notify the trigger of the event
			//console.debug(responseEvent);
			event.target.dispatchEvent(responseEvent);
			
			event.target.dispatchEvent(new CustomEvent("beidreadend"));
		}
		request.open("GET", requestURL, true);
		request.send();
	}
}

/** Notify the given elements if GesEtuEID is ready
 */
function GesEtuEidNotifyReady(elements)
{
	var requestURL = "http://localhost:8080/hello";
	var request = new XMLHttpRequest();
	
	request.responseType = 'json';
	request.onload = function(){
		//console.log("webext : beid ready");
		var responseEvent;
		if( this.status == 250 ){
			responseEvent = new CustomEvent("beidready");
			
			for(var i = 0; i < elements.length; i++){
				var element = elements[i];
				element.dispatchEvent(responseEvent);
			}
		}
	}
	request.open("GET", requestURL, true);
	request.send();
}

// Get all elements having the "gesetu-eid" class
var elements = document.getElementsByClassName("gesetu-eid");

// Assign listener GesEtuEidOnClick on "click" event of Elements
for(var i = 0; i < elements.length; i++){
	var element = elements[i];
	element.addEventListener("click", GesEtuEidOnClick, false);
}
// Notify the Elements if GesEtuEID is ready
GesEtuEidNotifyReady(elements);